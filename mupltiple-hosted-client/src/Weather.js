import Button from 'react-bootstrap/Button';
import {Container, Row, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import axios from 'axios';
import moment from 'moment'

function Weather() {
    const [weather, setWeather] = useState([])

    useEffect(() => {
        axios.get("https://localhost:5001/WeatherForecast")
            .then(response => {
                setWeather(response.data)
            })
            .catch(e => {
                console.log(e);
            });
    }, [])

    return (
      <Container>
          <Row>
              <Table striped bordered hover className="mx-auto my-5">
                  <thead>
                  <tr>
                      <th>Date</th>
                      <th>TemperatureC</th>
                      <th>TemperatureF</th>
                      <th>Summary</th>
                  </tr>
                  </thead>
                  <tbody>
                  {weather.map(x => <tr>
                      <td>{moment(x.date).format('MMMM Do YYYY, h:mm:ss a')}</td>
                      <td>{x.temperatureC}</td>
                      <td>{x.temperatureF}</td>
                      <td>{x.summary}</td>
                  </tr>)}
                  </tbody>
              </Table>
          </Row>
      </Container>
    );
}

export default Weather
